
import javax.swing.JOptionPane;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Brandon
 */
public class Values {

    int hm;
    double an;

    //function get how many times to add
    public int getHm() {
        return hm;
    }
    //method
    public void setHm(int hm) {
        this.hm = hm;
    }

    //function get number to add
    public double getAn() {
        return an;
    }
    //method
    public void setAn(double an) {
        this.an = an;
    }

}
